#!/bin/bash
set -e

BASEDIR=$(dirname $0)
cd $BASEDIR;

if [[ ! -e TOKEN ]];
then
	if [[ -n "$TOKEN" ]];
	then
		echo $TOKEN > TOKEN
	else
		echo 'You must set the environment variable $TOKEN to your telegram token'
		exit;
	fi
fi

python3 main.py
