import urllib.request, json
import datetime
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import telegram
import logging

##########
# CONFIG #
##########

HELP_MSG = "Dieser Bot ist um den Stundenplan, des dualen Studiengangs MATSE, in Telegram anzuzeigen.\n\nMomentan kann der Bot folgende Befehle, wobei [jahrgang] ∈ {1,2,3,4}, 4 entspricht dabei Wahlmodulen:\n/today[jahrgang] - Stundenplan heute\n/tomorrow[jahrgang] - Stundenplan morgen\n/thisweek[jahrgang] - Stundenplan diese Woche\n/nextweek[jahrgang] - Stundenplan nächste Woche\n/help - Diese Hilfenachricht\n/dsgvo - Unsere Datenschutzerklärung\n/license - Unsere Lizenz"

LICENSE_MSG = "Welcome!\nThis bot is a program which is available under the <b>GNU GPLv3</b> license at https://gitlab.com/Bergiu/MatseKalenderBot."

DSGVO_MSG = "Dieser Bot speichert <b>keine personenbezogenen Daten</b>."


##############
# FINAL VARS #
##############

cal_url_template = "https://www.matse.itc.rwth-aachen.de/stundenplan/web/eventFeed/{jahrgang}?start={start}&end={end}"


###################
# READ/SAVE FILES #
###################

def read_line_from_file(filename: str):
    """
    Loads a string from a file.
    """
    try:
        with open(filename, "r") as f:
            return f.readline().split("\n")[0]
    except FileNotFoundError:
        pass
    return


###########
# LOGGING #
###########

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)


##################
# DATE FUNCTIONS #
##################

def create_date_range(offset: int=0, length: int=1):
    """
    default create a date range for today
    offset: amount of days in the future
    length: amount of days added to the current
    """
    length -= 1  # length 1: one day
    firstday = datetime.date.today()
    if firstday != 0:
        firstday += datetime.timedelta(days=offset)
    lastday = firstday + datetime.timedelta(days=length)
    start = firstday.strftime("%Y-%m-%d")
    end = lastday.strftime("%Y-%m-%d")
    return (start, end)


def next_weekday(d, weekday):
    """
    d: date
    weekday: int, the next weekday, 0 = Monday, 1=Tuesday, 2=Wednesday...
    """
    days_ahead = weekday - d.weekday()
    if days_ahead <= 0: # Target day already happened this week
        days_ahead += 7
    return d + datetime.timedelta(days_ahead)


def create_date_range_weekly(offset: int=0, length: int=1):
    """
    default create a date range for the current week.
    offset: amount of weeks in the future
    length: amount of weeks added to the current
    """
    offset -= 1  # begin on the last monday in this week
    length -= 1  # length 1: one week
    d = datetime.date.today()
    next_monday = next_weekday(d, 0)
    next_monday += datetime.timedelta(days=7*offset)
    last_sunday = next_monday + datetime.timedelta(days=6)
    last_sunday += datetime.timedelta(days=7*length)
    return (next_monday, last_sunday)


#######################
# CONNECTION TO MATSE #
#######################

def download_cal(date_range, jahrgang: int):
    """
    date_range: string tuple (start, end)
    """
    cal_url = cal_url_template.format(jahrgang=jahrgang, start=date_range[0], end=date_range[1])
    events = []
    with urllib.request.urlopen(cal_url) as url:
        tmp = url.read().decode()
        events = json.loads(tmp)
    return events


def get_events(jahrgang, offset: int=0, length: int=1):
    date_range = create_date_range(offset, length)
    events = download_cal(date_range, jahrgang)
    return events


def get_events_weekly(jahrgang, offset: int=0, length: int=1):
    date_range = create_date_range_weekly(offset, length)
    events = download_cal(date_range, jahrgang)
    return events


#########
# VIEWS #
#########
def read_date(date):
    """
    convert matse reply date into a date object
    """
    datetime_object = datetime.datetime.strptime(date, "%Y-%m-%dT%H:%M:%S")
    return datetime_object

def prepare_dates(event):
    """
    change string dates to real dates
    """
    event["start"] = read_date(event["start"])
    event["end"] = read_date(event["end"])

def sort_events(events):
    return sorted(events, key=lambda d: d["start"])

def prepare_events(events):
    for event in events:
        prepare_dates(event)
    return sort_events(events)

def event_to_str(event):
    """
    a short view of an event
    """
    event_s = "<b>{title}</b>:\n  <i>{von}</i> - <i>{bis}</i> {ort}\n"
    von = event["start"].strftime("%H:%M")
    bis = event["end"].strftime("%H:%M")
    ort = event["location"]["name"]
    return event_s.format(title=event["title"], von=von, bis=bis, ort=ort)

def events_to_str(events):
    """
    short views of each event
    events: array of events
    """
    if len(events) == 0:
        return "Keine Veranstaltungen."
    out = ""
    events = prepare_events(events)
    for event in events:
        out += event_to_str(event)
    return out


def events_to_str_weekly(events):
    """
    events: array of events
    """
    if len(events) == 0:
        return "Keine Veranstaltungen."
    # sort by date
    events = prepare_events(events)
    # header
    first = events[0]["start"]
    last = events[len(events)-1]["end"]
    out = "🔸 KW " + first.strftime("%W")
    out += " (" + first.strftime("%d.%m") +  " - " + last.strftime("%d.%m") + ")"
    out += " 🔸\n\n"
    # add all events
    current_date = ""
    for event in events:
        # each day one heading with weekday
        next_date = event["start"].strftime("%a")
        if current_date != next_date:
            current_date = next_date
            out += "#" + current_date + ":\n"
        # then all events
        out += event_to_str(event)
    return out

def print_cal(events):
    print(events_to_str(events))


#############
# CLI VIEWS #
#############

def cli_today(jahrgang: int):
    events = get_events(jahrgang)
    print_cal(events)

def cli_tomorrow(jahrgang: int):
    print_cal(events)


################
# TELEGRAM BOT #
################

def reply_day_events(update, jahrgang, day_offset: int=0, day_length: int=1):
    events = get_events(jahrgang, day_offset, day_length)
    text = events_to_str(events)
    update.message.reply_text(text, parse_mode=telegram.ParseMode.HTML)


def reply_day_events_weekly(update, jahrgang, day_offset: int=0, day_length: int=1):
    events = get_events_weekly(jahrgang, day_offset, day_length)
    text = events_to_str_weekly(events)
    update.message.reply_text(text, parse_mode=telegram.ParseMode.HTML)


def today1(bot, update):
    reply_day_events(update, 1)

def today2(bot, update):
    reply_day_events(update, 2)

def today3(bot, update):
    reply_day_events(update, 3)

def today4(bot, update):
    reply_day_events(update, 4)


def tomorrow1(bot, update):
    reply_day_events(update, 1, 1)

def tomorrow2(bot, update):
    reply_day_events(update, 2, 1)

def tomorrow3(bot, update):
    reply_day_events(update, 3, 1)

def tomorrow4(bot, update):
    reply_day_events(update, 4, 1)


def thisweek1(bot, update):
    reply_day_events_weekly(update, 1)

def thisweek2(bot, update):
    reply_day_events_weekly(update, 2)

def thisweek3(bot, update):
    reply_day_events_weekly(update, 3)

def thisweek4(bot, update):
    reply_day_events_weekly(update, 4)


def nextweek1(bot, update):
    reply_day_events_weekly(update, 1, 1)

def nextweek2(bot, update):
    reply_day_events_weekly(update, 2, 1)

def nextweek3(bot, update):
    reply_day_events_weekly(update, 3, 1)

def nextweek4(bot, update):
    reply_day_events_weekly(update, 4, 1)


def license(bot, update):
    update.message.reply_text(LICENSE_MSG, parse_mode=telegram.ParseMode.HTML)

def dsgvo(bot, update):
    update.message.reply_text(DSGVO_MSG, parse_mode=telegram.ParseMode.HTML)

def _help(bot, update):
    update.message.reply_text(HELP_MSG, parse_mode=telegram.ParseMode.HTML)

def start(bot, update):
    _help(bot, update)


def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)


def main():
    token = read_line_from_file("TOKEN")
    """Start the bot."""
    updater = Updater(token)
    dp = updater.dispatcher

    # add handlers
    dp.add_handler(CommandHandler("today1", today1))
    dp.add_handler(CommandHandler("today2", today2))
    dp.add_handler(CommandHandler("today3", today3))
    dp.add_handler(CommandHandler("today4", today4))
    dp.add_handler(CommandHandler("tomorrow1", tomorrow1))
    dp.add_handler(CommandHandler("tomorrow2", tomorrow2))
    dp.add_handler(CommandHandler("tomorrow3", tomorrow3))
    dp.add_handler(CommandHandler("tomorrow4", tomorrow4))
    dp.add_handler(CommandHandler("thisweek1", thisweek1))
    dp.add_handler(CommandHandler("thisweek2", thisweek2))
    dp.add_handler(CommandHandler("thisweek3", thisweek3))
    dp.add_handler(CommandHandler("thisweek4", thisweek4))
    dp.add_handler(CommandHandler("nextweek1", nextweek1))
    dp.add_handler(CommandHandler("nextweek2", nextweek2))
    dp.add_handler(CommandHandler("nextweek3", nextweek3))
    dp.add_handler(CommandHandler("nextweek4", nextweek4))
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", _help))
    dp.add_handler(CommandHandler("license", license))
    dp.add_handler(CommandHandler("dsgvo", dsgvo))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT
    updater.idle()


if __name__ == '__main__':
    main()

