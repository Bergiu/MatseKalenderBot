# Matse Calendar Bot

Dieser Bot wurde für den Zweck programmiert, den Stundenplan des dualen Studiengangs bequem in Telegram anzuzeigen. Er läuft unter [@matsecalendar\_bot](https://t.me/matsecalendar_bot).

## Getting started
1. Um den Bot zu starten braucht man einen Bottoken. Diesen bekommt man vom [@botfather](https://t.me/botfather).
2. Beim botfather kann man die Befehle für den Bot einstellen. Dafür ist eine Liste der Befehle in `docs/commandlist.txt`. Diese einfach kopieren und beim Botfather für den Bot hinzufügen.
3. Weiter unten in der Readme bei Getting started von Docker oder Ohne Docker weiter machen.

## Docker
Für den Bot gibt es ein Dockerimage auf [hub.docker.com/r/bergiu/matse\_calendar\_bot/](https://hub.docker.com/r/bergiu/matse_calendar_bot/).

### Requirements
- docker

### Getting started
Man kann das dockerimage so starten:
- `docker run -e TOKEN="DEINTELEGRAMBOTTOKEN" -d bergiu/matse_calendar_bot:latest`

### Build
- `docker build -t my_matse_calendar_bot .`

### Run
- `docker run -e TOKEN="DEINTELEGRAMBOTTOKEN" -d my_matse_calendar_bot`

### Upload to dockerhub
- `docker push username/my_matse_calendar_bot`

## Ohne Docker
### Requirements
- python3
- [python-telegram-bot](https://github.com/python-telegram-bot/python-telegram-bot/)
	- `pip install python-telegram-bot --upgrade`

### Getting started
Den Bottoken in die Datei `TOKEN` schreiben. Danach kann man den Bot so starten:
- `python main.py`
