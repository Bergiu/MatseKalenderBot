FROM debian
MAINTAINER Marco Schlicht (marcoschlicht@onlinehome.de)

ENV DEBIAN_FRONTEND noninteractive

# install dependencies
RUN apt-get -qq update &&\
	apt-get -qq install apt-utils &&\
	apt-get -qq upgrade &&\
	apt-get -qq install python3 python3-pip
RUN pip3 install python-telegram-bot --upgrade

RUN mkdir /app
ADD main.py /app
ADD init.sh /app
RUN chmod +x /app/init.sh

WORKDIR /app
ENTRYPOINT /app/init.sh
